console.log('hello world')

// arithmetic operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("result of addition operator: " + sum);
let defference = x - y;
console.log("result of subtration operator: " + defference);
let product = x * y;
console.log("result of multiplecation operator: " + product);
let quatient= x / y;
console.log("result of Division operatpor: " + quatient);
let remainder = y % x;
console.log("result of modulo operatpor: " + remainder);

// Assignment operators "="
	// basic assignment operator
	let asssigmentNumber = 8;

	// addition assignment operator
		// addition assignment operator adds the value of the right operand to ta variable and assigns the result to the variable
		asssigmentNumber = asssigmentNumber + 2;
		console.log('result of addition operator:' + asssigmentNumber);
		asssigmentNumber += 2;
		console.log('result of addition operator:' + asssigmentNumber);

	// subtraction / multiplication / Division Assignment  Operator(-=, += , /=)
	asssigmentNumber -= 2;
		console.log('result of subtration operator:' + asssigmentNumber);

	asssigmentNumber *= 2;
		console.log('result of multiplication operator:' + asssigmentNumber);

	asssigmentNumber /= 2;
		console.log('result of Division operator:' + asssigmentNumber);

// multiple Operator and Parentheses
	/*
		-when multiple operator applied in a single statement, it follows the PEMDAS( Parentheses. exponents, multiplication, Division, addition , sub)
		1.3 * 4=12
		2.12 / 5 = 2.4
		3. 1 + 2 =3
		4. 3 - 2.4 =0.6


	*/

	let mdas = 1+2-3*4/5;
	console.log("result of mdas operator:" + mdas)

	let pemdas = 1+(2-3)*(4/5);
	console.log("result of pemdas operator:" + pemdas)

	/*
		-by addinf the parenthese "()" to create more complex computations will change the order of operations still ff the same rule

		1. 4/5 = 0.8
		2. 2 - 3 = -1
		3.-1 * 0.8 = -0.8
		4. 1 + -0.8 = .2
	
	*/

	// increment and decrement
		// operators that add or sub value by 1 and reassign the value of the variable where the increment/ decrement was applied

	let z = 1;
	//  the value of z is added by a value of one before returning the value and storing it in the variable "increment"
	let increment = ++z;
		console.log("result of pre-increment: " + increment)
		console.log("result of z: " + z)

	// the value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by 1

	increment = z++;
	// the value of "z" is at 2 before it was incremented
	console.log("result of post increment : " + increment)
	// the value of "z" was increased again reassigning the value to 3
	console.log("result of post increment : " + z )

	// decrement 
	let decrement = --z;
	console.log("result of pre-decrement: " + decrement)
	console.log("result of pre-decrement: " + z)

	// post decrement
	decrement = z --;
	console.log("result of post-decrement: " + decrement)
	console.log("result of post-decrement: " + z)

	// type coercion

	let numA = '10';
	let numB =  12 ;
	/*
		-adding / concatenating a string and a number will result in a string 
		-this can be proven in the console by looking at the color of the text desplayed
	*/
	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion)

	let numC = 16;
	let numD = 14;
	/*
		-the result is a number
		-this can be proven in the console by looking at the color of the text displayed

	*/
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion)

	/*
		-the result is a number
		-the boolean "true" is also associated w/ the value of 1

	*/

	let numE = true + 1 ;
	console.log(numE)


	/*
		-the result is a number 
		-the boolean "false" is also associated w/ the value of 0
	*/
	let numF = false + 1 ;
	console.log(numF)


	// comparison operators
	let  juan = 'juan';

	// Equality Operator (==)
		/*
			-checks wheher the operands are equal/have the same content
			-attempst to CONVERTS and COMPARE operands of different data types
			-returns a boolean value( true or false)
		*/
		console.log( 1==1 );//true
		console.log( 1==2 );//false
		console.log( 1=='1');//true
		console.log( 0==false);//true
	 	console.log( juan=='juan');//true
	 	console.log( 'juan'==juan);//true


	 	// inequality operator (!=)
	 	/*
			-check whether the operands are not equal/have diff content
			-attempts to CONVERT AND COMPARE operands of diff datatypes
	 	*/
	 		console.log( 1 != 1 );//False
			console.log( 1 != 2 );//true
			console.log( 1 != '1');//False
			console.log( 0 != false);//False
		 	console.log( juan!= 'juan');//False
		 	console.log( 'juan'!= juan);//False

		 // strict Equality Operator (===)
		 /*
			-it check whether the operands are equal/have the same content
			-also it compares the data types of 2 values
			-JS is a loosely type language meaning that values of diff data type can be stored in variables
			-strict equality operator are better to use in most cases to ensure  that data types are correct
		 */

		console.log( 1===1 );//true
		console.log( 1===2 );//false
		console.log( 1==='1');//false
		console.log( 0===false);//false
	 	console.log( 'juan'==='juan');//true
	 	console.log( 'juan'===juan);//false juan is declared in line 136

	 	// strict Inequality (!==)
	 	/*
			-checks whether the operands are not equal/have the same content
			-also compare the date type of 2 values
	 	*/
	 	console.log( 1!==1 );//false
		console.log( 1!==2 );//true
		console.log( 1!=='1');//true
		console.log( 0!==false);//true
	 	console.log( 'juan'!=='juan');//false
	 	console.log( 'juan'!==juan);//False

	 	// relational Operators
	 	/*


	 	*/
	 	let a=50;
	 	let b=65;

	 	let isGreaterThan = a > b;
	 	let isLessThan = a < b;
	 	let isGTorEqual = a >= b;
	 	let isLTorEqual = a <= b ;

	 	console.log(isGreaterThan)
	 	console.log(isLessThan)
	 	console.log(isGTorEqual)
	 	console.log(isLTorEqual)

	 	// coercion to change the string to a number
	 	let numStr = "30";
	 	console.log(a > numStr)//true
	  	console.log(a <= numStr)//false


	  	let str = "twenty";
	  	console.log(b>=str)//false
	  	// since the string is not numeric
	  	// the string was converter to number

	  	// Logical Operator (&&, ||, !)
		
		let isLegalAge =true;
		let isRegistered = false;

		// Logical and Operator ( && )
		// return true if all operands are true
		let allRequirementsMet = isLegalAge && isRegistered;
		console.log( 'Result of logical operator: ' + allRequirementsMet) //false

		// logical or Operator ( || )
		// Return True if one of the Operands are true
		let someRequirementsNotMet = isLegalAge || isRegistered;
		console.log( 'Result of logical operator: ' + someRequirementsNotMet) //true

		// logical not operator ( ! )
		// this return the opposite value
		someRequirementsNotMet = !isRegistered;
		console.log( 'Result of logical operator: ' + someRequirementsNotMet) //true


		// "typeof" operator is use to check date type of a value/expression and returns a string value of wht the data type is